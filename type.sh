#!/bin/bash

if [ "$1" == "q" ]; then
    speedpad --no-robot >> "$PWD"/stats/qwerty.log
fi

if [ "$1" == "d" ]; then
    speedpad --no-robot >> "$PWD"/stats/dvorak.log
fi
