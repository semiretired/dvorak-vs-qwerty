#!/bin/bash

for layout in `echo -n "qwerty dvorak"`
do
    gnuplot -e "
        set terminal pngcairo; 
        set xdata time; 
        set timefmt '%s'; 
        plot '$PWD/stats/$layout.log' using 1:15 smooth sbezier title 'WPM'" > "$PWD"/plots/"$layout"_time.png
done
